#include "mainwindow.h"

/*MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent)
{
    QWidget *zoneCentrale = new QWidget;
    m_colorPicker = new QColorDialog;
    QGridLayout *layout = new QGridLayout(this);

    accuracy = 100;
    m_linGrad = QLinearGradient(QPointF(0.5, 0), QPointF(379.5, 0));
    m_gradHolder = new QClickableGradient;
    QObject::connect(m_gradHolder, SIGNAL(clicked(int)), this, SLOT(onGradClicked(int)), Qt::QueuedConnection);
    m_colHolder = new QWidget;
    QGridLayout *tmpLayout = new QGridLayout(m_colHolder);
    m_colHolder->setLayout(tmpLayout);
    m_fcolHolder = new QWidget;
    QGridLayout *tmpLay  = new QGridLayout(m_fcolHolder);
    m_fcolHolder->setLayout(tmpLay);
    QPushButton *render = new QPushButton("Générer");
    QObject::connect(render, SIGNAL(clicked()), this, SLOT(generateFile()));
    refreshFinal();
    addColor(0.000, QColor(255,255,255), false);
    addColor(0.167, QColor(255,255,0), false);
    addColor(0.333, QColor(0,255,0), false);
    addColor(0.500, QColor(0,255,255), false);
    addColor(0.667, QColor(0,0,255), false);
    addColor(0.833, QColor(255,0,255), false);
    addColor(1.000, QColor(255,0,0));

    layout->addWidget(m_gradHolder,0,0);
    layout->addWidget(m_colHolder,2,0);
    layout->addWidget(m_fcolHolder,3,0);
    layout->addWidget(render,4,0);
    zoneCentrale->setLayout(layout);
    this->setCentralWidget(zoneCentrale);
    this->setWindowTitle("Gradworks (1.0.0)");
    this->setFixedWidth(400);
    this->setWindowIcon(QIcon("C:/Users/Florent/Documents/Qt/Gradworks Ressources/icon.png"));

    for (int i=0; i<129; i++) {
        QColor color = lpColor(i);
        nearestColor(color);
    } //useless
}*/
MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent)
{
    QWidget *zoneCentrale = new QWidget;
    m_colorPicker = new QColorDialog;
    QVBoxLayout *layout = new QVBoxLayout(zoneCentrale);
    layout->setAlignment(Qt::AlignTop);

    accuracy = 127;

    m_linGrad = QLinearGradient(QPointF(0.5, 0), QPointF(379.5, 0));
    m_gradHolder = new QClickableGradient;
    QObject::connect(m_gradHolder, SIGNAL(clicked(int)), this, SLOT(onGradClicked(int)), Qt::QueuedConnection);
    m_lAccuracy = new QLabel("Précision: 100%");
    QSlider *precision = new QSlider();
    precision->setMinimum(5);
    precision->setMaximum(500);
    precision->setValue(100);
    precision->setOrientation(Qt::Horizontal);
    QObject::connect(precision, SIGNAL(valueChanged(int)), this, SLOT(callRefresh(int)));
    m_colHolder = new QWidget;
    QGridLayout *tmpLayout = new QGridLayout(m_colHolder);
    m_colHolder->setLayout(tmpLayout);
    m_fcolHolder = new QWidget;
    QGridLayout *tmpLay  = new QGridLayout(m_fcolHolder);
    m_fcolHolder->setLayout(tmpLay);
    refreshFinal();
    addColor(0.000, QColor(255,255,255), false);
    addColor(0.167, QColor(255,255,0), false);
    addColor(0.333, QColor(0,255,0), false);
    addColor(0.500, QColor(0,255,255), false);
    addColor(0.667, QColor(0,0,255), false);
    addColor(0.833, QColor(255,0,255), false);
    addColor(1.000, QColor(255,0,0));

    layout->addWidget(m_gradHolder);
    layout->addWidget(m_lAccuracy);
    layout->addWidget(precision);
    layout->addWidget(m_colHolder);
    layout->addWidget(m_fcolHolder);
    zoneCentrale->setLayout(layout);
    this->setCentralWidget(zoneCentrale);
    this->setWindowTitle("Gradworks (1.1.0)");
    this->setFixedWidth(400);
    m_gradHolder->setFixedSize(m_gradHolder->sizeHint());
    m_lAccuracy->setFixedSize(m_lAccuracy->sizeHint());
}

/*void MainWindow::refreshFinal()
{
    int count = static_cast<int>(m_listCol.size());

    m_colExt.erase(m_colExt.begin(),m_colExt.end());
    m_colIn.erase(m_colIn.begin(),m_colIn.end());
    m_nbIn.erase(m_nbIn.begin(),m_nbIn.end());

    if (count == 0) {
        int j = 0;
        for (int i=0; i<127; i++) {
            QColor extColor = interpolate(QColor(255,255,255),QColor(0,0,0),i/127.0);
            m_colExt.push_back(extColor);
            int nbInCol = nearestColor(extColor);
            QColor inCol = lpColor(nbInCol);
            if (j>0)
            {
                if ((m_colIn[j-1].red() != inCol.red()) || (m_colIn[j-1].green() != inCol.green()) || (m_colIn[j-1].blue() != inCol.blue()))
                {
                    m_colIn.push_back(inCol);
                    m_nbIn.push_back(nbInCol);
                    j++;
                }
            } else {
                m_colIn.push_back(inCol);
                m_nbIn.push_back(nbInCol);
                j++;
            }
        }
    } else if (count == 1) {
        QColor extColor = m_listCol[0]->color();
        int nbInCol = nearestColor(extColor);
        QColor inCol = lpColor(nbInCol);
        for (int i=0; i<127; i++) {
            m_colExt.push_back(extColor);
        }
        m_colIn.push_back(inCol);
        m_nbIn.push_back(nbInCol);
    } else {
        int count = static_cast<int>(m_listCol.size());
        QColor gradCol [count];
        double gradInt [count];
        for (int i=0; i<count; i++)
        {
            gradCol[i] = m_listCol[i]->color();
            gradInt[i] = m_listCol[i]->pos();
        }
        int extMin = (int)(nearestPos(gradInt[0])*127);
        int extMax = (int)(nearestPos(gradInt[count-1])*127+1);

        QColor minCol = gradCol[0];
        int nMinCol = nearestColor(minCol);
        QColor inMinCol = lpColor(nMinCol);
        for (int i=0; i<extMin; i++) {
            m_colExt.push_back(minCol);
        }
        m_colIn.push_back(inMinCol);
        m_nbIn.push_back(nMinCol);

        for (int h=1; h<count; h++) {
            int extbMin = (int)(nearestPos(gradInt[h-1])*127);
            int extbMax = (int)(nearestPos(gradInt[h])*127);

            double x1 = gradInt[h-1];
            double x2 = gradInt[h];
            double segmentLength = sqrt((x2-x1)*(x2-x1));
            int j = static_cast<int>(m_colIn.size());
            for (int i=extbMin; i<extbMax; i++) {
                double xp = nearestPos(i/127.0);
                double pdist = sqrt((xp-x1)*(xp-x1));
                double ratio = pdist/segmentLength;
                QColor extColor = interpolate(gradCol[h],gradCol[h-1],ratio);
                m_colExt.push_back(extColor);
                int nbInCol = nearestColor(extColor);
                QColor inCol = lpColor(nbInCol);
                if ((m_colIn[j-1].red() != inCol.red()) || (m_colIn[j-1].green() != inCol.green()) || (m_colIn[j-1].blue() != inCol.blue()))
                {
                    m_colIn.push_back(inCol);
                    m_nbIn.push_back(nbInCol);
                    j++;
                }
            }
        }

        QColor maxCol = gradCol[count-1];
        int nMaxCol = nearestColor(maxCol);
        QColor inMaxCol = lpColor(nMaxCol);
        for (int i=extMax-1; i<127; i++) {
            m_colExt.push_back(maxCol);
        }
        int inMax = static_cast<int>(m_colIn.size());
        if ((m_colIn[inMax-1].red() != inMaxCol.red()) || (m_colIn[inMax-1].green() != inMaxCol.green()) || (m_colIn[inMax-1].blue() != inMaxCol.blue()))
        {
            m_colIn.push_back(inMaxCol);
            m_nbIn.push_back(nMaxCol);
        }
    }


    delete m_fcolHolder->layout();
    for (int i=0; i<static_cast<int>(m_listfCol.size()); i++) {
        delete m_listfCol[i];
    }
    m_listfCol.erase(m_listfCol.begin(),m_listfCol.end());
    QGridLayout *layout  = new QGridLayout(m_fcolHolder);
    for (int i=0; i<static_cast<int>(m_colIn.size()); i++) {
        //for (int i=0; i<static_cast<int>(m_colExt.size()); i++) {
        QLabel *col = new QLabel;
        QPixmap *pixmap = new QPixmap(26,51);
        pixmap->fill(QColor("transparent"));
        QPainter colorPainter(pixmap);
        colorPainter.setRenderHint(QPainter::Antialiasing);
        QPainterPath colorPath;
        colorPainter.setPen(QPen(Qt::black, 1));
        colorPath.addRoundedRect(QRectF(0.5, 0.5, 25, 25), 2, 2);
        colorPainter.setFont(QFont("Arial"));
        //colorPainter.fillPath(colorPath, Qt::darkGray);
        colorPainter.drawPath(colorPath);
        colorPainter.setPen(QColor("transparent"));
        //colorPath.addRoundedRect(QRectF(4, 18, 18, 13), 2, 2);
        colorPainter.fillPath(colorPath, m_colIn[i]);
        colorPainter.drawPath(colorPath);
        colorPainter.setPen(QPen(Qt::darkGray,1));
        colorPainter.drawText(4, 28 ,18, 13, Qt::AlignCenter, QString::number(m_nbIn[i]));

        col->setPixmap(*pixmap);
        m_listfCol.push_back(col);
        layout->addWidget(col,i/10,i%10);
    }
    m_fcolHolder->setLayout(layout);
}*/
void MainWindow::refreshFinal()
{
    int count = static_cast<int>(m_listCol.size());

    m_colExt.erase(m_colExt.begin(),m_colExt.end());
    m_colIn.erase(m_colIn.begin(),m_colIn.end());
    m_nbIn.erase(m_nbIn.begin(),m_nbIn.end());

    if (count == 0) {
        int j = 0;
        for (int i=0; i<accuracy; i++) {
            QColor extColor = interpolate(QColor(255,255,255),QColor(0,0,0),i/double(accuracy));
            m_colExt.push_back(extColor);
            int nbInCol = nearestColor(extColor);
            QColor inCol = lpColor(nbInCol);
            if (j>0)
            {
                if ((m_colIn[j-1].red() != inCol.red()) || (m_colIn[j-1].green() != inCol.green()) || (m_colIn[j-1].blue() != inCol.blue()))
                {
                    m_colIn.push_back(inCol);
                    m_nbIn.push_back(nbInCol);
                    j++;
                }
            } else {
                m_colIn.push_back(inCol);
                m_nbIn.push_back(nbInCol);
                j++;
            }
        }
    } else if (count == 1) {
        QColor extColor = m_listCol[0]->color();
        int nbInCol = nearestColor(extColor);
        QColor inCol = lpColor(nbInCol);
        for (int i=0; i<accuracy; i++) {
            m_colExt.push_back(extColor);
        }
        m_colIn.push_back(inCol);
        m_nbIn.push_back(nbInCol);
    } else {
        int count = static_cast<int>(m_listCol.size());
        QColor gradCol [count];
        double gradInt [count];
        for (int i=0; i<count; i++)
        {
            gradCol[i] = m_listCol[i]->color();
            gradInt[i] = m_listCol[i]->pos();
        }
        int extMin = (int)(nearestPos(gradInt[0])*accuracy);
        int extMax = (int)(nearestPos(gradInt[count-1])*accuracy+1);

        QColor minCol = gradCol[0];
        int nMinCol = nearestColor(minCol);
        QColor inMinCol = lpColor(nMinCol);
        for (int i=0; i<extMin; i++) {
            m_colExt.push_back(minCol);
        }
        m_colIn.push_back(inMinCol);
        m_nbIn.push_back(nMinCol);

        for (int h=1; h<count; h++) {
            int extbMin = (int)(nearestPos(gradInt[h-1])*accuracy);
            int extbMax = (int)(nearestPos(gradInt[h])*accuracy);

            double x1 = gradInt[h-1];
            double x2 = gradInt[h];
            double segmentLength = sqrt((x2-x1)*(x2-x1));
            int j = static_cast<int>(m_colIn.size());
            for (int i=extbMin; i<extbMax; i++) {
                double xp = nearestPos(i/double(accuracy));
                double pdist = sqrt((xp-x1)*(xp-x1));
                double ratio = pdist/segmentLength;
                QColor extColor = interpolate(gradCol[h],gradCol[h-1],ratio);
                m_colExt.push_back(extColor);
                int nbInCol = nearestColor(extColor);
                QColor inCol = lpColor(nbInCol);
                if ((m_colIn[j-1].red() != inCol.red()) || (m_colIn[j-1].green() != inCol.green()) || (m_colIn[j-1].blue() != inCol.blue()))
                {
                    m_colIn.push_back(inCol);
                    m_nbIn.push_back(nbInCol);
                    j++;
                }
            }
        }

        QColor maxCol = gradCol[count-1];
        int nMaxCol = nearestColor(maxCol);
        QColor inMaxCol = lpColor(nMaxCol);
        for (int i=extMax-1; i<accuracy; i++) {
            m_colExt.push_back(maxCol);
        }
        int inMax = static_cast<int>(m_colIn.size());
        if ((m_colIn[inMax-1].red() != inMaxCol.red()) || (m_colIn[inMax-1].green() != inMaxCol.green()) || (m_colIn[inMax-1].blue() != inMaxCol.blue()))
        {
            m_colIn.push_back(inMaxCol);
            m_nbIn.push_back(nMaxCol);
        }
    }


    delete m_fcolHolder->layout();
    for (int i=0; i<static_cast<int>(m_listfCol.size()); i++) {
        delete m_listfCol[i];
    }
    m_listfCol.erase(m_listfCol.begin(),m_listfCol.end());
    QGridLayout *layout  = new QGridLayout(m_fcolHolder);
    for (int i=0; i<static_cast<int>(m_colIn.size()); i++) {
        QLabel *col = new QLabel;
        QPixmap *pixmap = new QPixmap(26,51);
        pixmap->fill(QColor("transparent"));
        QPainter colorPainter(pixmap);
        colorPainter.setRenderHint(QPainter::Antialiasing);
        QPainterPath colorPath;
        colorPainter.setPen(QPen(Qt::black, 1));
        colorPath.addRoundedRect(QRectF(0.5, 0.5, 25, 25), 2, 2);
        colorPainter.setFont(QFont("Arial"));
        colorPainter.drawPath(colorPath);
        colorPainter.setPen(QColor("transparent"));
        colorPainter.fillPath(colorPath, m_colIn[i]);
        colorPainter.drawPath(colorPath);
        colorPainter.setPen(QPen(Qt::darkGray,1));
        colorPainter.drawText(4, 28 ,18, 13, Qt::AlignCenter, QString::number(m_nbIn[i]));

        col->setPixmap(*pixmap);
        m_listfCol.push_back(col);
        layout->addWidget(col,i/10,i%10);
    }
    m_fcolHolder->setLayout(layout);
    m_fcolHolder->setFixedSize(400,10+ceil(static_cast<int>(m_listfCol.size())/10.0)*65);
}

void MainWindow::addColor(const double& pos, const QColor& color, const bool build)
{
    delete m_colHolder->layout();
    QPen pen(Qt::black, 1);
    QClickableColor *col = new QClickableColor("",m_colHolder,pos,color);
    QPixmap *pixmap = new QPixmap(51,26);
    pixmap->fill(QColor("transparent"));
    QPainter colorPainter(pixmap);
    colorPainter.setRenderHint(QPainter::Antialiasing);
    QPainterPath colorPath;
    colorPath.addRoundedRect(QRectF(0.5, 0.5, 50, 25), 2, 2);
    colorPainter.setPen(pen);
    colorPainter.fillPath(colorPath, color);
    colorPainter.drawPath(colorPath);
    col->setPixmap(*pixmap);
    QObject::connect(col, SIGNAL(clicked()), this, SLOT(onColorClicked()), Qt::QueuedConnection);
    m_listCol.push_back(col);
    QGridLayout *layout = new QGridLayout(m_colHolder);
    std::sort(m_listCol.begin(), m_listCol.end(),
              [](const QClickableColor* l, const QClickableColor* r) {
        return l->pos() < r->pos(); });
    for (int i=0; i<static_cast<int>(m_listCol.size()); i++)
    {
        layout->addWidget(m_listCol[i],i/5,i%5);
    }
    m_colHolder->setLayout(layout);
    m_colHolder->setFixedSize(400,10+ceil(static_cast<int>(m_listCol.size())/5.0)*35);
    m_linGrad.setColorAt(pos, color);
    if (build)
    {
        QPixmap *grad = new QPixmap(400,80);
        grad->fill(QColor("transparent"));
        QPainter painterGrad(grad);
        painterGrad.setRenderHint(QPainter::Antialiasing);
        QPainterPath pathGrad;
        pathGrad.addRoundedRect(QRectF(0.5, 0.5, 380, 60), 2, 2);
        painterGrad.setPen(pen);
        painterGrad.fillPath(pathGrad, m_linGrad);
        painterGrad.drawPath(pathGrad);
        m_gradHolder->setPixmap(*grad);
    }
    refreshFinal();
}

void MainWindow::onColorClicked()
{
    QClickableColor *sndr = qobject_cast<QClickableColor *>( this->sender() );
    double pos = sndr->pos();
    QColor color = sndr->color();
    auto it = find_if(m_listCol.begin(), m_listCol.end(), [&pos](const QClickableColor* obj) {return obj->pos() == pos;});
    if (it != m_listCol.end())
    {
        auto index = std::distance(m_listCol.begin(), it);
        if (m_listCol[index]->color() == color) {
            delete m_colHolder->layout();
            delete m_listCol[index];
            m_linGrad = QLinearGradient(QPointF(0.5, 0), QPointF(379.5, 0));
            m_listCol.erase(m_listCol.begin() + index);
            QGridLayout *layout = new QGridLayout(m_colHolder);
            for (int i=0; i<static_cast<int>(m_listCol.size()); i++)
            {
                layout->addWidget(m_listCol[i],i/5,i%5);
                m_linGrad.setColorAt(m_listCol[i]->pos(), m_listCol[i]->color());
            }
            m_colHolder->setLayout(layout);
            m_colHolder->setFixedSize(400,10+ceil(static_cast<int>(m_listCol.size())/5.0)*35);
            QPen pen(Qt::black, 1);
            QPixmap *grad = new QPixmap(400,80);
            grad->fill(QColor("transparent"));
            QPainter painterGrad(grad);
            painterGrad.setRenderHint(QPainter::Antialiasing);
            QPainterPath pathGrad;
            pathGrad.addRoundedRect(QRectF(0.5, 0.5, 380, 60), 2, 2);
            painterGrad.setPen(pen);
            painterGrad.fillPath(pathGrad, m_linGrad);
            painterGrad.drawPath(pathGrad);
            m_gradHolder->setPixmap(*grad);
            refreshFinal();
        }
    }
}

void MainWindow::onGradClicked(int pos)
{
    QColor color = QColorDialog::getColor(getColor(double(pos)/381), this);
    if(color.isValid())
    {
        addColor(double(pos)/381, color);
    }
}

/*double MainWindow::nearestPos(double pos)
{
    int x = floor(pos*127);
    return x/127.0;
}*/
double MainWindow::nearestPos(double pos)
{
    int x = floor(pos*accuracy);
    return x/double(accuracy);
}

int MainWindow::nearestColor(QColor &color)
{
    double r = (color.red()/255.0);
    double g = (color.green()/ 255.0);
    double b = (color.blue()/ 255.0);

    if (r>0.04045) {
        r = pow((r+0.055)/1.055,2.4);
    } else {
        r = r/12.92;
    }
    if (g>0.04045) {
        g = pow((g+0.055)/1.055,2.4);
    } else {
        g = g/12.92;
    }
    if (b>0.04045) {
        b = pow((b+0.055)/1.055,2.4);
    }
    else {
        b = b/12.92;
    }
    r = r * 100;
    g = g * 100;
    b = b * 100;

    double x = (r * 0.4124 + g * 0.3576 + b * 0.1805)/94.811;
    double y = (r * 0.2126 + g * 0.7152 + b * 0.0722)/100.000;
    double z = (r * 0.0193 + g * 0.1192 + b * 0.9505)/107.304;

    if (x>0.008856) {
        x = cbrt(x);
    } else {
        x = (7.787*x)+(16/116);
    }
    if (y>0.008856) {
        y = cbrt(y);
    } else {
        y = (7.787*y)+(16/116);
    }
    if (z>0.008856) {
        z = cbrt(z);
    } else {
        z = (7.787*z)+(16/116);
    }

    double CIEL = (116*y)-16;
    double CIEa = 500*(x-y);
    double CIEb = 200*(y-z);

    double colors [128][3] = {{-16,0,0},
                              {11.2636,0.0986617,-0.231905},
                              {53.1928,0.250395,-0.588556},
                              {100,0.419782,-0.986699},
                              {58.9287,67.4248,39.5482},
                              {53.2329,80.4231,66.9655},
                              {16.1242,37.3165,52.5882},
                              {-14.1331,8.41663,2.94642},
                              {81.1473,15.5894,49.119},
                              {59.5262,62.7658,69.6056},
                              {20.0542,26.4158,57.2385},
                              {10.669,2.48955,43.5166},
                              {97.35,-19.4623,79.8328},
                              {97.1382,-21.1695,93.9915},
                              {36.4964,-9.82269,43.6123},
                              {8.14625,-73.3394,39.6773},
                              {90.4485,-62.5039,70.7053},
                              {88.6404,-77.8648,83.8139},
                              {32.7138,-34.7718,39.0847},
                              {14.8252,-17.907,48.7715},
                              {88.7228,-76.2458,68.3891},
                              {87.737,-85.8854,82.7142},
                              {32.1342,-39.851,38.3795},
                              {-9.71976,-12.7942,9.14618},
                              {88.8585,-74.6083,61.6229},
                              {87.771,-85.4417,80.35},
                              {32.1994,-39.0033,34.1509},
                              {-9.68017,-12.5149,8.37698},
                              {89.3152,-69.2414,43.3037},
                              {88.0532,-81.8088,64.1115},
                              {32.3326,-37.3069,27.3076},
                              {9.18689,-83.9588,32.7105},
                              {90.0796,-60.7183,20.8553},
                              {88.8373,-72.2199,34.2257},
                              {32.7046,-32.8078,14.1054},
                              {-9.32527,-10.011,1.48169},
                              {74.6902,-14.2037,-40.0899},
                              {66.21,-5.36892,-53.4698},
                              {24.9203,-11.2829,-15.8445},
                              {-12.0186,-2.34645,-35.0228},
                              {58.3817,19.4767,-65.8815},
                              {43.8243,46.0852,-89.6583},
                              {13.2302,17.5338,-39.1832},
                              {-13.7973,1.2772,-37.6194},
                              {44.1257,55.4282,-89.0839},
                              {32.3026,79.4349,-108.797},
                              {-9.48484,105.381,-77.8911},
                              {-15.366,4.47301,-12.3178},
                              {48.9331,63.1847,-81.0146},
                              {36.1853,80.9388,-102.218},
                              {10.0058,40.6158,-51.8711},
                              {-13.155,17.7358,-54.5472},
                              {64.6829,85.927,-54.8308},
                              {60.3199,98.6084,-61.7823},
                              {19.4126,45.7545,-28.6671},
                              {-13.4991,12.8896,-9.37136},
                              {60.0493,71.2495,6.72203},
                              {53.9208,82.2958,28.8118},
                              {16.5654,38.5141,9.50933},
                              {-12.5034,16.846,-3.40315},
                              {53.8102,78.7539,67.1642},
                              {36.5904,40.1492,48.0428},
                              {37.7522,10.8929,46.1621},
                              {38.389,-25.9656,44.2685},
                              {19.8248,-28.8777,54.6628},
                              {31.9735,-31.9867,13.1246},
                              {33.7169,-4.52904,-30.8199},
                              {32.3026,79.4349,-108.797},
                              {26.2179,-14.8669,-11.8785},
                              {26.019,67.5451,-89.9349},
                              {53.1928,0.250395,-0.588556},
                              {12.25,0.102231,-0.240296},
                              {53.2329,80.4231,66.9655},
                              {92.8075,-46.4595,82.895},
                              {86.8473,-44.8154,83.4508},
                              {89.0401,-74.4553,83.6202},
                              {50.1867,-53.4923,52.8289},
                              {88.576,-75.3378,42.698},
                              {66.21,-5.36892,-53.4698},
                              {35.7443,68.9323,-103.048},
                              {34.5528,80.2366,-104.982},
                              {40.1763,83.0211,-95.474},
                              {40.7101,64.8036,-17.3658},
                              {16.3935,12.2691,51.7835},
                              {58.1863,66.4268,68.9681},
                              {81.3453,-53.8403,78.3141},
                              {89.4639,-70.9235,82.9793},
                              {87.737,-85.8854,82.7142},
                              {88.2517,-80.9929,78.6779},
                              {89.3162,-70.2099,54.0676},
                              {90.1928,-58.4243,10.1396},
                              {59.5449,20.5039,-63.9797},
                              {38.9244,30.0404,-65.0853},
                              {58.2385,29.1752,-53.6924},
                              {53.9714,89.4383,-72.3776},
                              {54.0803,82.7261,23.5841},
                              {66.8538,43.6559,73.5733},
                              {70.4309,-12.0561,72.0692},
                              {90.528,-62.7241,86.1003},
                              {42.2607,8.7564,47.9146},
                              {18.3544,1.7509,53.9062},
                              {27.7232,-30.9078,28.6688},
                              {29.6843,-26.5814,8.15176},
                              {-8.20524,75.8836,-42.1541},
                              {15.1787,18.1622,-36.6336},
                              {30.203,16.9185,27.3949},
                              {34.6948,58.9588,44.7052},
                              {53.7622,54.2137,40.4405},
                              {57.0665,39.1795,58.3573},
                              {89.5101,-6.09571,83.5964},
                              {82.553,-45.0439,72.6683},
                              {66.4938,-47.3821,64.8125},
                              {12.0767,5.41911,-12.2202},
                              {95.0755,-31.3671,64.8164},
                              {91.5876,-49.6711,20.0834},
                              {67.1813,24.922,-51.5939},
                              {54.5259,51.0887,-71.8889},
                              {27.0934,0.155947,-0.366554},
                              {49.239,0.236087,-0.554925},
                              {97.8681,-9.54063,-4.35956},
                              {32.8616,56.7592,74.3971},
                              {-9.16313,95.1577,10.7905},
                              {73.1095,-72.3387,71.1118},
                              {23.4774,-31.188,58.5796},
                              {70.4309,-12.0561,72.0692},
                              {21.0314,1.09031,57.1412},
                              {49.2137,29.3017,57.4168},
                              {15.678,24.7174,50.5115}};

    //qDebug() << CIEL << CIEa << CIEb;

    double delta = 10000.0;
    int nb = -1;
    for (int i=0; i<128; i++)
    {
        double tmp = sqrt((colors[i][0]-CIEL)*(colors[i][0]-CIEL) + (colors[i][1]-CIEa)*(colors[i][1]-CIEa) + (colors[i][2]-CIEb)*(colors[i][2]-CIEb));
        if (tmp < delta) {
            delta = tmp;
            nb = i;
        }
    }
    //EasyRGB

    return nb;
}

QColor MainWindow::lpColor(int nb)
{
    double rgb [128][3] = {{0,0,0},
                           {30,30,30},
                           {127,127,127},
                           {255,255,255},
                           {255,76,76},
                           {255,0,0},
                           {89,0,0},
                           {25,0,0},
                           {255,189,108},
                           {255,84,0},
                           {89,29,0},
                           {39,27,0},
                           {255,255,76},
                           {255,255,0},
                           {89,89,0},
                           {25,25,0},
                           {136,255,76},
                           {84,255,0},
                           {29,89,0},
                           {20,43,0},
                           {76,255,76},
                           {0,255,0},
                           {0,89,0},
                           {0,25,0},
                           {76,255,94},
                           {0,255,25},
                           {0,89,13},
                           {0,25,2},
                           {76,255,136},
                           {0,255,85},
                           {0,89,29},
                           {0,31,18},
                           {76,255,183},
                           {0,255,153},
                           {0,89,53},
                           {0,25,18},
                           {76,195,255},
                           {0,169,255},
                           {0,65,82},
                           {0,16,25},
                           {76,136,255},
                           {0,85,255},
                           {0,29,89},
                           {0,8,25},
                           {76,76,255},
                           {0,0,255},
                           {0,0,89},
                           {0,0,25},
                           {135,76,255},
                           {84,0,255},
                           {25,0,100},
                           {15,0,48},
                           {255,76,255},
                           {255,0,255},
                           {89,0,89},
                           {25,0,25},
                           {255,76,135},
                           {255,0,84},
                           {89,0,29},
                           {34,0,19},
                           {255,21,0},
                           {153,53,0},
                           {121,81,0},
                           {67,100,0},
                           {3,57,0},
                           {0,87,53},
                           {0,84,127},
                           {0,0,255},
                           {0,69,79},
                           {37,0,204},
                           {127,127,127},
                           {32,32,32},
                           {255,0,0},
                           {189,255,45},
                           {175,237,6},
                           {100,255,9},
                           {16,139,0},
                           {0,255,135},
                           {0,169,255},
                           {0,42,255},
                           {63,0,255},
                           {122,0,255},
                           {178,26,125},
                           {64,33,0},
                           {255,74,0},
                           {136,225,6},
                           {114,255,21},
                           {0,255,0},
                           {59,255,38},
                           {89,255,113},
                           {56,255,204},
                           {91,138,255},
                           {49,81,198},
                           {135,127,233},
                           {211,29,255},
                           {255,0,93},
                           {255,127,0},
                           {185,176,0},
                           {144,255,0},
                           {131,93,7},
                           {57,43,0},
                           {20,76,16},
                           {13,80,56},
                           {21,21,42},
                           {22,32,90},
                           {105,60,28},
                           {168,0,10},
                           {222,81,61},
                           {216,106,28},
                           {255,225,38},
                           {158,225,47},
                           {103,181,15},
                           {30,30,48},
                           {220,255,107},
                           {128,255,189},
                           {154,153,255},
                           {142,102,255},
                           {64,64,64},
                           {117,117,117},
                           {224,255,255},
                           {160,0,0},
                           {53,0,0},
                           {26,208,0},
                           {7,66,0},
                           {185,176,0},
                           {63,49,0},
                           {179,95,0},
                           {75,21,2}};
    return QColor(rgb[nb][0],rgb[nb][1],rgb[nb][2]);
}

/*QColor MainWindow::getColor(double value)
{
    int count = static_cast<int>(m_listCol.size());
    QColor gradCol [count];
    double gradInt [count];
    for (int i=0; i<count; i++)
    {
        gradCol[i] = m_listCol[i]->color();
        gradInt[i] = m_listCol[i]->pos();
    }
    int interval=count-1;

    double extMin = nearestPos(gradInt[0]);
    double extMax = nearestPos(gradInt[count-1])+1/127;

    for (int i=1; i<count;i++)
    {
        if(value<=gradInt[i] ){interval=i;break;}
    }
    double x1 = gradInt[interval-1];
    double x2 = gradInt[interval];
    double xp = value;
    double pdist = sqrt((xp-x1)*(xp-x1));
    double segmentLength = sqrt((x2-x1)*(x2-x1));
    double ratio = pdist/segmentLength;
    QColor color;
    if (count > 1)
    {
        if (value < extMin) {
            color = gradCol[0];
        } else if (value > extMax) {
            color = gradCol[count-1];
        } else {
            color = interpolate(gradCol[interval],gradCol[interval-1],ratio);
        }
    }
    else
    {
        if (count == 0)
        {
            color = interpolate(QColor(255,255,255),QColor(0,0,0),value);
        }
        else
        {
            color = gradCol[0];
        }
    }
    return color;
}*/
QColor MainWindow::getColor(double value)
{
    int count = static_cast<int>(m_listCol.size());
    QColor gradCol [count];
    double gradInt [count];
    for (int i=0; i<count; i++)
    {
        gradCol[i] = m_listCol[i]->color();
        gradInt[i] = m_listCol[i]->pos();
    }
    int interval=count-1;

    double extMin = nearestPos(gradInt[0]);
    double extMax = nearestPos(gradInt[count-1])+1/accuracy;

    for (int i=1; i<count;i++)
    {
        if(value<=gradInt[i] ){interval=i;break;}
    }
    double x1 = gradInt[interval-1];
    double x2 = gradInt[interval];
    double xp = value;
    double pdist = sqrt((xp-x1)*(xp-x1));
    double segmentLength = sqrt((x2-x1)*(x2-x1));
    double ratio = pdist/segmentLength;
    QColor color;
    if (count > 1)
    {
        if (value < extMin) {
            color = gradCol[0];
        } else if (value > extMax) {
            color = gradCol[count-1];
        } else {
            color = interpolate(gradCol[interval],gradCol[interval-1],ratio);
        }
    }
    else
    {
        if (count == 0)
        {
            color = interpolate(QColor(255,255,255),QColor(0,0,0),value);
        }
        else
        {
            color = gradCol[0];
        }
    }
    return color;
}

QColor MainWindow::interpolate(QColor start,QColor end,double ratio)
{
    int r = (int)(ratio*start.red() + (1-ratio)*end.red());
    int g = (int)(ratio*start.green() + (1-ratio)*end.green());
    int b = (int)(ratio*start.blue() + (1-ratio)*end.blue());
    return QColor::fromRgb(r,g,b);
}

/*void MainWindow::generateFile()
{
    QString filename = "Colors.py";
    QFile file(filename);
    if (file.open(QIODevice::ReadWrite)) {
        QTextStream stream(&file);
        QString colors = "";
        stream << "from _Framework.ButtonElement import Color\n"
                  "from .consts import BLINK_LED_CHANNEL, PULSE_LED_CHANNEL\n\n"

                  "class Blink(Color):\n\n"

                  "    def __init__(self, midi_value=0, *a, **k):\n"
                  "        super(Blink, self).__init__(midi_value, *a, **k)\n\n"

                  "    def draw(self, interface):\n"
                  "        interface.send_value(0)\n"
                  "        interface.send_value(self.midi_value, channel=BLINK_LED_CHANNEL)\n\n\n"


                  "class Pulse(Color):\n\n"

                  "    def __init__(self, midi_value=0, *a, **k):\n"
                  "        super(Pulse, self).__init__(midi_value, *a, **k)\n\n"

                  "    def draw(self, interface):\n"
                  "        interface.send_value(0)\n"
                  "        interface.send_value(self.midi_value, channel=PULSE_LED_CHANNEL)\n\n\n"


                  "class Rgb:\n"
                  "    BLACK = Color(0)\n"
                  "    DARK_GREY = Color(1)\n"
                  "    GREY = Color(2)\n"
                  "    WHITE = Color(3)\n"
                  "    RED = Color(5)\n"
                  "    RED_BLINK = Blink(5)\n"
                  "    RED_PULSE = Pulse(5)\n"
                  "    RED_HALF = Color(7)\n"
                  "    ORANGE = Color(9)\n"
                  "    ORANGE_HALF = Color(11)\n"
                  "    AMBER = Color(96)\n"
                  "    AMBER_HALF = Color(14)\n"
                  "    YELLOW = Color(13)\n"
                  "    YELLOW_HALF = Color(15)\n"
                  "    DARK_YELLOW = Color(17)\n"
                  "    DARK_YELLOW_HALF = Color(19)\n"
                  "    GREEN = Color(21)\n"
                  "    GREEN_BLINK = Blink(21)\n"
                  "    GREEN_PULSE = Pulse(21)\n"
                  "    GREEN_HALF = Color(27)\n"
                  "    MINT = Color(29)\n"
                  "    MINT_HALF = Color(31)\n"
                  "    LIGHT_BLUE = Color(37)\n"
                  "    LIGHT_BLUE_HALF = Color(39)\n"
                  "    BLUE = Color(45)\n"
                  "    BLUE_HALF = Color(47)\n"
                  "    DARK_BLUE = Color(49)\n"
                  "    DARK_BLUE_HALF = Color(51)\n"
                  "    PURPLE = Color(53)\n"
                  "    PURPLE_HALF = Color(55)\n"
                  "    DARK_ORANGE = Color(84)\n\n\n"


                  "CLIP_COLOR_TABLE = {15549221: 60,12411136: 61,\n"
                  "   11569920: 62,\n"
                  "   8754719: 63,\n"
                  "   5480241: 64,\n"
                  "   695438: 65,\n"
                  "   31421: 66,\n"
                  "   197631: 67,\n"
                  "   3101346: 68,\n"
                  "   6441901: 69,\n"
                  "   8092539: 70,\n"
                  "   3947580: 71,\n"
                  "   16712965: 72,\n"
                  "   12565097: 73,\n"
                  "   10927616: 74,\n"
                  "   8046132: 75,\n"
                  "   4047616: 76,\n"
                  "   49071: 77,\n"
                  "   1090798: 78,\n"
                  "   5538020: 79,\n"
                  "   8940772: 80,\n"
                  "   10701741: 81,\n"
                  "   12008809: 82,\n"
                  "   9852725: 83,\n"
                  "   16149507: 84,\n"
                  "   12581632: 85,\n"
                  "   8912743: 86,\n"
                  "   1769263: 87,\n"
                  "   2490280: 88,\n"
                  "   6094824: 89,\n"
                  "   1698303: 90,\n"
                  "   9160191: 91,\n"
                  "   9611263: 92,\n"
                  "   12094975: 93,\n"
                  "   14183652: 94,\n"
                  "   16726484: 95,\n"
                  "   16753961: 96,\n"
                  "   16773172: 97,\n"
                  "   14939139: 98,\n"
                  "   14402304: 99,\n"
                  "   12492131: 100,\n"
                  "   9024637: 101,\n"
                  "   8962746: 102,\n"
                  "   10204100: 103,\n"
                  "   8758722: 104,\n"
                  "   13011836: 105,\n"
                  "   15810688: 106,\n"
                  "   16749734: 107,\n"
                  "   16753524: 108,\n"
                  "   16772767: 109,\n"
                  "   13821080: 110,\n"
                  "   12243060: 111,\n"
                  "   11119017: 112,\n"
                  "   13958625: 113,\n"
                  "   13496824: 114,\n"
                  "   12173795: 115,\n"
                  "   13482980: 116,\n"
                  "   13684944: 117,\n"
                  "   14673637: 118,\n"
                  "   16777215: 119\n"
                  "   }\n"
                  "RGB_COLOR_TABLE = (\n"
                  " (0, 0),\n";
        for (int i=0; i<static_cast<int>(m_colExt.size()); i++)
        {
            QString str = m_colExt[i].name();
            str.remove(0, 1);
            int hex = str.toInt(NULL, 16);
            if (i==static_cast<int>(m_colExt.size())-1) {
                stream << " (" << i+1 << ", " << hex << "))";
                break;
            }
            stream << " (" << i+1 << ", " << hex << "),\n";
        }
    }
}*/

void MainWindow::callRefresh(int value)
{
    accuracy = value*127/100;
    m_lAccuracy->setText(QString("Précision: %1%").arg(value));
    refreshFinal();
}

MainWindow::~MainWindow()
{
}
