#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#define MIN(a, b) ((a < b) ? a : b)
#define MAX(a, b) ((a > b) ? a : b)

#include <QtWidgets/QtWidgets>
#include <vector>
#include <iostream>
#include <string>
#include "qclickablecolor.h"
#include "qclickablegradient.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    bool operator()(const QClickableColor* c1, const QClickableColor* c2) const
    {
        return c1->pos() < c2->pos();
    }
    std::vector< QColor > m_colIn;
    std::vector< double > m_posIn;
    std::vector< int > m_nbIn;
    std::vector< QColor > m_colExt;
    std::vector< double > m_posExt;
    void refreshFinal();
    QColor getColor(double value);
    double nearestPos(double pos);
    int nearestColor(QColor &color);
    QColor lpColor(int nb);
    QColor interpolate(QColor start,QColor end,double ratio);
    ~MainWindow();

public slots:
    void onColorClicked();
    void onGradClicked(int pos);
    //void generateFile();
    void callRefresh(int value);

private:
    void addColor(const double& pos, const QColor& color, const bool build=true);
    std::vector< QClickableColor* > m_listCol;
    std::vector< QLabel* > m_listfCol;
    QColorDialog *m_colorPicker;
    QLinearGradient m_linGrad;
    QClickableGradient *m_gradHolder;
    QWidget *m_colHolder;
    QWidget *m_fcolHolder;
    QLabel *m_lAccuracy;
    int accuracy;
};

#endif // MAINWINDOW_H
