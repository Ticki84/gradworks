#include "qclickablecolor.h"

QClickableColor::QClickableColor(const QString& text, QWidget* parent, const double& pos, const QColor& color)
    : QLabel(parent), m_pos(pos), m_color(color)
{
    setText(text);
}

QClickableColor::~QClickableColor()
{
}

void QClickableColor::mousePressEvent(QMouseEvent* event)
{
    emit clicked();
}

double QClickableColor::pos() const
{
    return m_pos;
}

QColor QClickableColor::color()
{
    return m_color;
}
