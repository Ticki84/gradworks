#ifndef QCLICKABLECOLOR_H
#define QCLICKABLECOLOR_H

#include <QtWidgets/QtWidgets>


class QClickableColor : public QLabel
{
Q_OBJECT
public:
    explicit QClickableColor( const QString& text="", QWidget* parent=0, const double& pos=0, const QColor& color=QColor(255,255,255));
    double pos() const;
    QColor color();
    ~QClickableColor();
private:
    double m_pos;
    QColor m_color;
signals:
    void clicked();
protected:
    void mousePressEvent(QMouseEvent* event);
};

#endif // QCLICKABLECOLOR_H
