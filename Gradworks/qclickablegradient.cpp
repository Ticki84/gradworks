#include "qclickablegradient.h"

QClickableGradient::QClickableGradient(const QString& text, QWidget* parent)
    : QLabel(parent)
{
    setText(text);
}

QClickableGradient::~QClickableGradient()
{
}

void QClickableGradient::mousePressEvent(QMouseEvent* event)
{
    emit clicked(event->localPos().x());
}
