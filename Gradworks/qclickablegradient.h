#ifndef QCLICKABLEGRADIENT_H
#define QCLICKABLEGRADIENT_H

#include <QtWidgets/QtWidgets>


class QClickableGradient : public QLabel
{
Q_OBJECT
public:
    explicit QClickableGradient( const QString& text="", QWidget* parent=0);
    ~QClickableGradient();
signals:
    void clicked(int pos);
protected:
    void mousePressEvent(QMouseEvent* event);
};

#endif // QCLICKABLEGRADIENT_H
